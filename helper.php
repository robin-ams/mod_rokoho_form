<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );


$doc = JFactory::getDocument();
$doc->addScript('modules/mod_rokoho_form/js/jquery.form-validator.min.js');



class modFormHelper
{

	public function getContactEmail($id) {
		// Get a db connection.
		$db = JFactory::getDbo();
		 
		// Create a new query object.
		$query = $db->getQuery(true);


		// $query->select($db->quoteName(array('name', 'con_position', 'address', 'suburb', 'state', 'country', 'postcode', 'telephone','fax', 'image', 'mobile','webpage')));
		$query->select($db->quoteName(array('name', 'email_to')));
		$query->from($db->quoteName('#__contact_details'));
		$query->where($db->quoteName('id')." = ". $id);


		// Reset the query using our newly populated query object.
		$db->setQuery($query);

		$row = $db->loadAssoc();

		return $row;

	}



	public function sendmail($from,$name,$to,$subject,$message) {
	$application = JFactory::getApplication();

		$mailer = JFactory::getMailer();
		$config = JFactory::getConfig();
		
		$sender = array($from,$name);
		 
		$mailer->setSender($sender);
		$mailer->addRecipient($to);
		
		$body   = $message;
		$mailer->setSubject($subject);
		$mailer->setBody($body);
		
		$send = $mailer->Send();
		
		if ( $send !== true ) {
			$application->enqueueMessage( 'Error sending email: ' , 'warning');
		} 
		else {
			$application->enqueueMessage(JText::_('FORM_MAIL_SENT'), 'success'); 
		}
	}
}
?>